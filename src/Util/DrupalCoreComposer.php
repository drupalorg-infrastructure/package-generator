<?php

namespace PackageGenerator\Util;

/**
 * Utilities for accessing composer.json data from drupal/drupal and drupal/core.
 *
 * Some data is stored in the root composer.json file, while others is found
 * in the core/composer.json file.
 */
class DrupalCoreComposer {

  /**
   * Path to the drupal/drupal repository.
   *
   * @var string
   */
  protected $repositoryPath;

  /**
   * Array of caches to json data (composer.json, composer.lock, etc.)
   *
   * @var array[]
   */
  protected $jsonDataCache = [];

  /**
   * DrupalCoreComposer constructor.
   *
   * @param string $repositoryPath
   *   Path to the drupal/drupal repository.
   */
  public function __construct(string $repositoryPath) {
    $this->repositoryPath = $repositoryPath;
  }

  /**
   * Fetch the composer data from the root drupal/drupal project.
   *
   * @return array
   *   Composer json data
   */
  public function rootComposerJson() {
    return $this->jsonData('composer.json');
  }

  /**
   * Fetch the composer lock data.
   *
   * @return array
   *   Composer lock data
   */
  public function composerLock() {
    return $this->jsonData('composer.lock');
  }

  /**
   * Fetch the composer data from drupal/core.
   *
   * @return array
   *   Core Composer json data
   */
  public function coreComposerJson() {
    return $this->jsonData('core/composer.json');
  }

  /**
   * Return the path to the drupal/drupal repository root.
   *
   * @return string
   *   Path to the repository root.
   */
  public function repositoryPath() {
    return $this->repositoryPath;
  }

  /**
   * Determine if drupal/core contains scaffold data or not.
   *
   * @return bool
   *   True if scaffold data is present
   */
  public function hasScaffoldData() {
    $coreComposerJsonData = $this->coreComposerJson();
    return isset($coreComposerJsonData['extra']['drupal-scaffold']);
  }

  /**
   * Load the json data from the specified path.
   *
   * Cache the file internally once it has been loaded.
   *
   * @param string $path
   *   Relative path to the json file to load.
   *
   * @return array
   *   The contents of the json data for the specified file.
   */
  protected function jsonData($path) {
    if (!isset($this->jsonDataCache[$path])) {
      // Fetch the existing composer.json for the drupal/core project.
      $fullPath = $this->repositoryPath . '/' . $path;
      $this->jsonDataCache[$path] = file_exists($fullPath) ? json_decode(file_get_contents($fullPath), TRUE) : [];
    }
    return $this->jsonDataCache[$path];
  }

  /**
   * Return the "require-dev" section from root or core composer.json file.
   *
   * The require-dev constraints moved from core/composer.json (8.7.x and
   * earlier) to the root composer.json file (8.8.x and later).
   *
   * @return array
   *   The contents of the "require-dev" section.
   */
  public function getRequireDev() {
    $composerJsonData = $this->coreComposerJson();
    if (!empty($composerJsonData['require-dev'])) {
      return $composerJsonData['require-dev'];
    }

    $composerJsonData = $this->rootComposerJson();
    return isset($composerJsonData['require-dev']) ? $composerJsonData['require-dev'] : [];
  }

}
