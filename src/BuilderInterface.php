<?php

namespace PackageGenerator;

use PackageGenerator\Util\DrupalCoreComposer;

/**
 * Produce the output (commit message and files) for one generated commit.
 *
 * BuilderInterface provides an interface for builder claases which  are
 * called by the Dumper in order to produce a derived metapackage from the
 * provided source package. The dumper will create a new builder for every
 * commit it processes, so this object is immutable.
 *
 * The builder class is responsible for two things:
 *
 * - getCommitMessage: A commit message for the current branch or tag
 * - getPackage: The contents of the composer.json file for the new metapackage
 */
interface BuilderInterface {

  /**
   * BuilderInterface constructor.
   *
   * @param \PackageGenerator\Util\DrupalCoreComposer $drupalCoreInfo
   *   Information about the composer.json, composer.lock, and repository path.
   * @param string $referenceName
   *   The name of the branch or tag being processed.
   * @param string $commitHash
   *   The sha hash of the current commit being processed.
   * @param array $config
   *   Various configuration values.
   */
  public function __construct(DrupalCoreComposer $drupalCoreInfo, string $referenceName, string $commitHash, array $config);

  /**
   * Generate a commit message for the current tag or branch.
   *
   * @return string
   *   Commit message contents.
   */
  public function getCommitMessage();

  /**
   * Generate the Composer.json data for the current tag or branch.
   *
   * @return array
   *   Composer json data.
   */
  public function getPackage();

}
