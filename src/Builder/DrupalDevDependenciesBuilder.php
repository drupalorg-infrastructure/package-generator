<?php

namespace PackageGenerator\Builder;

/**
 * Builder to produce metapackage for drupal/core-dev.
 */
class DrupalDevDependenciesBuilder extends DrupalPackageBuilder {

  /**
   * Generate the composer.json data for drupal/core-dev.
   */
  public function getPackage() {
    $composer = $this->config['composer']['metadata'] + ['require' => []];

    // Don't mix with older version of this metapackage.
    // n.b. We must not conflict with drupal/core-dev-pinned, as that
    // interferes with the process we go through when generating tarballs.
    $composer['conflict']['webflo/drupal-core-require-dev'] = '*';

    // Put everything from Drupal's "require-dev" into our "require" section.
    $composer['require'] = $this->drupalCoreInfo->getRequireDev();

    // If the require-dev is bringing in a dev version of behat/mink, convert
    // the requirement to a more flexible set of versions.
    if (isset($composer['require']['behat/mink']) && ($composer['require']['behat/mink'] == '1.7.x-dev')) {
      $composer['require']['behat/mink'] = '1.8.0 | 1.7.1.1 | 1.7.x-dev';
    }

    // Do the same sort of conversion for behat/mink-selenium2-driver.
    if (isset($composer['require']['behat/mink-selenium2-driver']) && ($composer['require']['behat/mink-selenium2-driver'] == '1.3.x-dev')) {
      $composer['require']['behat/mink-selenium2-driver'] = '1.4.0 | 1.3.1.1 | 1.3.x-dev';
    }

    // Sort our required packages by key.
    ksort($composer['require']);

    return $composer;
  }

}
