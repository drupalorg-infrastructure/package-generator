<?php

namespace PackageGenerator\Builder;

use PackageGenerator\FilePlacementInterface;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Builder to produce metapackage for drupal/legacy-scaffold-assets.
 */
class DrupalLegacyScaffoldAssetsBuilder extends DrupalPackageBuilder implements FilePlacementInterface {

  /**
   * Generate the composer.json data for drupal/legacy-scaffold-assets.
   */
  public function getPackage() {
    $composer = $this->config['composer']['metadata'];

    // Always require "drupal/core": "self.version".
    $composer['require']['drupal/core'] = 'self.version';

    // If drupal/core already has 'drupal-scaffold' entries, then
    // exit early without adding our own file mappings. Allow 'drupal/core'
    // to scaffold in case the project-level composer.json does not.
    if ($this->drupalCoreInfo->hasScaffoldData()) {
      // TODO: If we implicitly allow drupal/core and drupal/legacy-scaffold-assets,
      // in the scaffold plugin, then we do not need to allow drupal/core to scaffold here.
      $composer['extra']['drupal-scaffold']['allowed-packages'] = ['drupal/core'];
      return $composer;
    }

    // Generate file mappings and scaffold assets for the current version of drupal/drupal.
    $composer['extra']['drupal-scaffold']['file-mapping'] = $this->addFileMapping();

    return $composer;
  }

  /**
   * Generate the scaffold file-mappings for the composer.json data.
   */
  protected function addFileMapping() {
    $fs = new Filesystem();

    $scaffold_file_mapping = $this->config['composer']['file-mapping'];

    // The file mappings are structured as follows:
    //
    // - "[web-root]/sites/example.settings.local.php": "assets/example.settings.local.php"
    //
    // The key is the path to the asset in the Drupal project, and the value
    // is the path to the asset in the Scaffold Assets.
    // The Drupal project path is where we read the asset from now, as we are
    // building the Scaffold Assets. It is also where the asset will ultimately
    // be installed to when the Drupal Scaffold operation actually executes.
    // The scaffold assets path is the location where we will write the asset
    // to as we build the Scaffold Assets project.
    foreach ($scaffold_file_mapping as $drupal_project_asset_path => $scaffold_asset_path) {
      if (is_array($scaffold_asset_path)) {
        if (isset($scaffold_asset_path['shadow'])) {
          unset($scaffold_file_mapping[$drupal_project_asset_path]['shadow']);
          $drupal_project_asset_path = $scaffold_asset_path['shadow'];
        }
        $scaffold_asset_path = $scaffold_asset_path['path'];
      }
      $source_path = str_replace(['[web-root]', '[project-root]'], $this->drupalCoreInfo->repositoryPath(), $drupal_project_asset_path);
      // If the source scaffold file does not exist in this revision, then
      // remove it from the file mapping path.
      if (!file_exists($source_path)) {
        unset($scaffold_file_mapping[$drupal_project_asset_path]);
      }
    }

    return $scaffold_file_mapping;
  }

  /**
   * Create the scaffold file assets corresponding to file-mappings info.
   *
   * @param array $builderComposerJson
   *   Composer json data created by builder (from getPackage).
   * @param string $targetBaseDir
   *   Target directory where files should be written.
   *
   * @return string[]
   *   List of relative paths to files created.
   */
  public function placeFiles(array $builderComposerJson, string $targetBaseDir) {
    $fs = new Filesystem();

    // Exit early if there is no file-mapping element.
    if (empty($builderComposerJson['extra']['drupal-scaffold']['file-mapping'])) {
      return [];
    }

    $scaffold_file_mapping = $builderComposerJson['extra']['drupal-scaffold']['file-mapping'];

    $placed = [];

    // Look up the file-mapping inforrmation.
    foreach ($scaffold_file_mapping as $drupal_project_asset_path => $scaffold_asset_path) {
      $source_path = str_replace(['[web-root]', '[project-root]'], $this->drupalCoreInfo->repositoryPath(), $drupal_project_asset_path);
      $target_path = $targetBaseDir . '/' . $scaffold_asset_path;
      $fs->mkdir(dirname($target_path));
      copy($source_path, $target_path);
      $placed[] = $scaffold_asset_path;
    }

    return $placed;
  }

}
