<?php

namespace PackageGenerator\Builder;

use PackageGenerator\BuilderInterface;
use PackageGenerator\Util\DrupalCoreComposer;

/**
 * Base class that includes helpful utility routine for Drupal builder classes.
 */
abstract class DrupalPackageBuilder implements BuilderInterface {

  /**
   * Information about composer.json, composer.lock etc. in current release.
   *
   * @var \PackageGenerator\Util\DrupalCoreComposer
   */
  protected $drupalCoreInfo;

  /**
   * Symbolic name of the branch or tag being processed.
   *
   * @var string
   */
  protected $referenceName;

  /**
   * SHA hash of the current commit.
   *
   * @var string
   */
  protected $commitHash;

  /**
   * Various configuration options.
   *
   * @var array
   */
  protected $config;

  /**
   * DrupalPackageBuilder constructor.
   *
   * @param \PackageGenerator\Util\DrupalCoreComposer $drupalCoreInfo
   *   Information about composer.json and composer.lock from current release.
   * @param string $referenceName
   *   Name of the release (branch or tag) being processed.
   * @param string $commitHash
   *   Commit of the tag or HEAD of the branch being processed.
   * @param array $config
   *   Various configuration information.
   */
  public function __construct(DrupalCoreComposer $drupalCoreInfo, string $referenceName, string $commitHash, array $config) {
    $this->drupalCoreInfo = $drupalCoreInfo;
    $this->refereceName = $referenceName;
    $this->commitHash = $commitHash;
    $this->config = $config;
  }

  /**
   * {@inheritdoc}
   */
  public function getCommitMessage() {
    $msg[] = "Update composer.json ({$this->referenceName})";
    $msg[] = '';
    $msg[] = 'Reference: https://git.drupalcode.org/project/drupal/commit/' . $this->commitHash;

    return implode("\n", $msg);
  }

  /**
   * Look up locked version from the package information.
   *
   * @param array $package
   *   Information about dependency.
   *
   * @return string
   *   Locked version, potentially converted from dev to equiv. stable release.
   */
  public function packageToVersion(array $package) {
    // We should never has dev versions in our composer.lock file. If we
    // find one, try to convert it to an equivalent stable release.
    if (substr($package['version'], 0, 4) == 'dev-') {
      return $this->convertDevReference($package['name'], $package['version'], $package['source']['reference']);
    }
    return $package['version'];
  }

  /**
   * Convert a dev sha hash to an equivalent stalbe release, if available.
   *
   * Convert from one of the dev-version sha hashes recorded in Drupal's
   * composer.lock between version 8.3.0-alpha1 and 8.7.7 to an equivalent
   * stable release that is as close as possible to the locked commit.
   *
   *   8.3.0-alpha1 - 8.3.0-rc2:
   *     "behat/mink": "dev-master#12e09bf56d4892998518eff6e9be897ba23b7dc1"
   *   8.3.0+:
   *     "behat/mink": "dev-master#9ea1cebe3dc529ba3861d87c818f045362c40484"
   *   8.5.0+:
   *     behat/mink-selenium2-driver": "dev-master#93474c65a2a7bf959200ab5f7a14cc450645c185"
   *
   * IMPORTANT NOTE: THe stable tags for the commits above have not been made
   * yet, so we have to use a development branch instead. This does not work in
   * the general case, because persistent development branches are usually
   * not available for most projects, so use caution before duplicating this pattern.
   *
   * There are also a few dev dependencies for very old versions of Drupal 8.0.0:
   *
   *   8.0.0-beta16:
   *     behat/mink-goutte-driver:dev-master#cc5ce119b5a8e06662f634b35967aff0b0c7dfdd
   *   8.0.1 - 8.0.4:
   *     jcalderonzumba/gastonjs:dev-master#5e231b4df98275c404e1371fc5fadd34f6a121ad
   *     jcalderonzumba/mink-phantomjs-driver:dev-master#10d7c48c9a4129463052321b52450d98983c4332
   *
   * We fix these up for completeness only. There are also a number of dev
   * dependencies for pre-8.0.0 releases. We make no attempt to repair these.
   *
   * @param string $name
   *   Name of dependency that is locked to a development version.
   * @param string $version
   *   The verison that the dependency is locked to.
   * @param string $sha
   *   Commit sha hash of development version.
   *
   * @return string
   *   Dev reference converted to a stable release, if possible.
   */
  protected function convertDevReference($name, $version, $sha) {
    $conversionTable = [
      'behat/mink:dev-master#a534fe7dac9525e8e10ca68e737c3d7e5058ec83' => '1.8.0 | 1.7.1.1 | 1.7.x-dev',
      'behat/mink:dev-master#12e09bf56d4892998518eff6e9be897ba23b7dc1' => '1.8.0 | 1.7.1.1 | 1.7.x-dev',
      'behat/mink:dev-master#9ea1cebe3dc529ba3861d87c818f045362c40484' => '1.8.0 | 1.7.1.1 | 1.7.x-dev',

      'behat/mink-selenium2-driver:dev-master#8684ee4e634db7abda9039ea53545f86fc1e105a' => '1.4.0 | 1.3.1.1 | 1.3.x-dev',
      'behat/mink-selenium2-driver:dev-master#93474c65a2a7bf959200ab5f7a14cc450645c185' => '1.4.0 | 1.3.1.1 | 1.3.x-dev',

      'jcalderonzumba/gastonjs:dev-master#5e231b4df98275c404e1371fc5fadd34f6a121ad' => '1.0.1',
      'jcalderonzumba/mink-phantomjs-driver:dev-master#10d7c48c9a4129463052321b52450d98983c4332' => '0.3.1',
    ];

    // Return the item from the lookup table if we can find it.
    $key = "$name:$version#$sha";
    if (isset($conversionTable[$key])) {
      return $conversionTable[$key];
    }

    // Warn if we find a dev dependency in a stable release. Ignore unresolved
    // dev dependencies in non-stable releases.
    if (preg_match('#^[0-9]+\.[0-9]+\.[0-9]+$#', $this->referenceName)) {
      print ">>> Could not find $key in conversion table for {$this->referenceName}\n";
    }

    // We will put in a reference to the source sha commit hash if we fail
    // to remap; however, this is for documentation purposes only, as
    // commit references are ignored, except at the top-level composer.json.
    // Ideally, we never get here.
    return "$version#$sha";
  }

}
