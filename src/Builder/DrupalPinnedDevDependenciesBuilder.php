<?php

namespace PackageGenerator\Builder;

/**
 * Builder to produce metapackage for drupal/core-dev-pinned.
 */
class DrupalPinnedDevDependenciesBuilder extends DrupalPackageBuilder {

  /**
   * Generate the composer.json data for drupal/core-dev-pinned.
   */
  public function getPackage() {
    $composer = $this->config['composer']['metadata'];

    // Always require "drupal/core": "self.version".
    $composer['require']['drupal/core'] = 'self.version';

    // Don't mix with older version of this metapackage.
    // n.b. We must not conflict with drupal/core-dev, as that
    // interferes with the process we go through when generating tarballs.
    $composer['conflict']['webflo/drupal-core-require-dev'] = '*';

    // Pull the exact versions of the dependencies from the composer.lock
    // file and use it to build our 'require' section.
    $composerLockData = $this->drupalCoreInfo->composerLock();
    if (isset($composerLockData['packages-dev'])) {
      foreach ($composerLockData['packages-dev'] as $package) {
        $composer['require'][$package['name']] = $this->packageToVersion($package);
      }
    }

    return $composer;
  }

}
