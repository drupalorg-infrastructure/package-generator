<?php

namespace PackageGenerator\Builder;

use Composer\Semver\Semver;

/**
 * Builder to produce metapackage for drupal/core-recommended.
 */
class DrupalCoreRecommendedBuilder extends DrupalPackageBuilder {

  /**
   * Generate the composer.json data for drupal/core-recommended.
   */
  public function getPackage() {
    $composer = $this->config['composer']['metadata'];

    // Require "drupal/core": "self.version" if drupal/core has scaffold files;
    // require "drupal/legacy-scaffold-assets" if it does not.
    // n.b. drupal/legacy-scaffold-assets requires drupal/core.
    $chainedProject = 'drupal/core';
    if (!$this->drupalCoreInfo->hasScaffoldData()) {
      $chainedProject = 'drupal/legacy-scaffold-assets';
    }
    $composer['require'][$chainedProject] = 'self.version';

    // Don't mix with older version of this metapackage.
    $composer['conflict']['webflo/drupal-core-strict'] = '*';

    // Pull up the composer lock data.
    $composerLockData = $this->drupalCoreInfo->composerLock();
    if (!isset($composerLockData['packages'])) {
      return $composer;
    }

    $remove_list = $this->findRemoveList($composerLockData);

    // Copy the 'packages' section from the Composer lock into our 'require'
    // section. There is also a 'packages-dev' section, but we do not need
    // to pin 'require-dev' versions, as 'require-dev' dependencies are never
    // included from subprojects. Use 'drupal/core-dev' to get
    // Drupal's dev dependencies.
    foreach ($composerLockData['packages'] as $package) {
      // If there is no 'source' record, then this is a path repository or something.
      if (isset($package['source']) && !in_array($package['name'], $remove_list)) {
        $composer['require'][$package['name']] = $this->packageToVersion($package);
      }
    }

    return $composer;
  }

  /**
   * Find the appropriate 'remove' list from the composer configuration data.
   *
   * There may be multiple 'remove' lists; each one is keyed by the version
   * constraints for the version of drupal/core that it should be used with.
   *
   * @param array $composerLockData
   *   Data from composer.lock file.
   *
   * @return array
   *   List of projects to remove.
   */
  protected function findRemoveList(array $composerLockData) {
    if (!isset($this->config['composer']['remove'])) {
      return [];
    }

    $drupal_core_version = $this->findPackageVersion($composerLockData, 'drupal/core');

    foreach ($this->config['composer']['remove'] as $versionConstraint => $removeList) {
      if (Semver::satisfies($drupal_core_version, $versionConstraint)) {
        return $removeList;
      }
    }

    return [];
  }

  /**
   * Search the provided composer lock data for the version of a package.
   *
   * @param array $composerLockData
   *   Data from composer.lock file.
   * @param string $name
   *   Name of the package to search for.
   *
   * @return string
   *   Version of the specified package.
   */
  protected function findPackageVersion(array $composerLockData, $name) {
    foreach ($composerLockData['packages'] as $package) {
      if (($package['name'] == $name) && isset($package['version'])) {
        return $package['version'];
      }
    }
    return '1.0.0';
  }

}
