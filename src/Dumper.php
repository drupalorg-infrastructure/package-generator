<?php

namespace PackageGenerator;

use Gitonomy\Git\Reference;
use Gitonomy\Git\Reference\Branch;
use Gitonomy\Git\Reference\Tag;
use Gitonomy\Git\Repository;

/**
 * Dumper is responsible for writing the target metapackage.
 *
 * A separate dumper instance is created for every branch and every tag that
 * exists in the source project. A 'builder' object is provided to actually
 * do the work of producing the contents of each tag and branch; the dumper
 * will take the result and commit it to the target metapackage.
 *
 * - For every branch, a new commit is written if the contents of the
 *   generated composer.json changes.
 * - For every tag, if the same tag does not exist in the target project,
 *   then a new tag is generated, committed and tagged in the target project.
 *
 * The allowRewrite() method can be used to write every tag in the source
 * repository, incuding tags that already exist. If allowRewrite() is not
 * used, then the dumper will exit without doing anything if the target tag
 * already exists.
 */
class Dumper {

  /**
   * Reference to the current branch or tag.
   *
   * @var \Gitonomy\Git\Reference\Tag|\Gitonomy\Git\Reference\Branch
   */
  protected $reference;

  /**
   * Reference to the target repository.
   *
   * @var \Gitonomy\Git\Repository
   */
  protected $repository;

  /**
   * Name of current branch.
   *
   * @var string
   */
  protected $branch;

  /**
   * Reference to current tag, or NULL if processing a branch.
   *
   * @var \Gitonomy\Git\Reference\Tag
   */
  protected $tag;

  /**
   * Symbolic name of the current tag or branch.
   *
   * @var string
   */
  protected $name;

  /**
   * Builder to use for this target.
   *
   * @var \PackageGenerator\BuilderInterface
   */
  protected $builder;

  /**
   * Record whether it is okay to rewrite existing tags.
   *
   * @var bool
   */
  protected $rewriteAllowed = FALSE;

  /**
   * Dumper constructor.
   *
   * @param \Gitonomy\Git\Reference $reference
   *   Reference to the branch or tag being processed.
   * @param BuilderInterface $builder
   *   Builder being used to create the output for the target tag or branch.
   * @param \Gitonomy\Git\Repository $repository
   *   Target repository that results will be written to.
   */
  public function __construct(Reference $reference, BuilderInterface $builder, Repository $repository) {

    if ($reference instanceof Tag || $reference instanceof Branch) {
      $this->reference = $reference;
    }
    else {
      throw new \InvalidArgumentException('$ref is not a tag or branch.');
    }

    $this->builder = $builder;
    $this->repository = $repository;
  }

  /**
   * Specify whether dumper should process tags that already exist.
   *
   * @param bool $rewriteAllowed
   *   Set to 'true' to allow existing tags to be overwritten.
   *
   * @return $this
   */
  public function allowRewrite($rewriteAllowed = TRUE) {
    $this->rewriteAllowed = $rewriteAllowed;
    return $this;
  }

  /**
   * Determines whether the reference is a branch or a tag.
   *
   * @param \Gitonomy\Git\Reference $reference
   *   Reference to the branch or tag being processed.
   */
  protected function getBranch(Reference $reference) {
    if ($reference instanceof Branch) {
      $this->branch = str_replace('origin/', '', $reference->getName());
      $this->tag = NULL;
      $this->name = $this->branch;
    }
    elseif ($reference instanceof Tag) {
      $branch = explode('.', $reference->getName());
      $this->branch = implode('.', [$branch[0], $branch[1], 'x']);
      $this->tag = $reference;
      $this->name = $this->tag->getName();
    }
  }

  /**
   * Calls the builder to produce the current step, and writes it to the target.
   */
  public function write() {
    $this->getBranch($this->reference);
    $wc = $this->repository->getWorkingCopy();
    if (!$this->repository->getReferences()->hasRemoteBranch('origin/' . $this->branch)) {
      if ($this->repository->getReferences()->hasBranch($this->branch)) {
        $this->repository->run('checkout', [$this->branch]);
      }
      else {
        $this->repository->run('checkout', ['--orphan', $this->branch]);
      }
      $this->repository->run('rm', ['--cached', '-r', '-f', '.']);
    }
    else {
      $wc->checkout($this->branch);
    }

    // Tag already exists.
    if (isset($this->tag) && $this->repository->getReferences()->hasTag($this->tag->getName()) && !$this->rewriteAllowed) {
      return;
    }

    $package = $this->builder->getPackage();

    file_put_contents($this->repository->getPath() . '/composer.json', json_encode($package, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
    $this->repository->run('add', ['composer.json']);

    // If the builder implements FilePlacementInterface, allow it to place files.
    if ($this->builder instanceof FilePlacementInterface) {
      $placed = $this->builder->placeFiles($package, $this->repository->getPath());
      foreach ($placed as $path) {
        $this->repository->run('add', [$path]);
      }
    }

    // Force the tag creation if we are rewriting the repository.
    $tagOptions = [];
    if ($this->rewriteAllowed) {
      $tagOptions = ['-f'];
    }

    $commitMessage = $this->builder->getCommitMessage();

    if (isset($this->tag)) {
      $this->repository->run('commit', [
        '--allow-empty',
        '-m',
        $commitMessage
      ]);
      $this->repository->run('tag', array_merge([$this->tag->getName()], $tagOptions));
    }
    elseif (!empty($wc->getDiffStaged()->getFiles())) {
      $this->repository->run('commit', ['-m', $commitMessage]);
    }
  }

}
