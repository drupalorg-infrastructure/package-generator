<?php

namespace PackageGenerator;


use Symfony\Component\Console\Output\BufferedOutput;

/**
 * Convenience class for creating fixtures.
 */
class Fixtures {

  /**
   * Gets the path to the project fixtures.
   *
   * @return string
   *   Path to project fixtures
   */
  public function allFixturesDir() {
    return dirname(__DIR__) . '/fixtures';
  }

  /**
   * Gets the path to one particular project fixture.
   *
   * @param string $project_name
   *   The project name to get the path for.
   *
   * @return string
   *   Path to project fixture.
   */
  public function projectPath($project_name) {
    $dir = $this->allFixturesDir() . '/' . $project_name;
    if (!is_dir($dir)) {
      throw new \RuntimeException("Requested fixture project {$project_name} that does not exist.");
    }
    return $dir;
  }

  /**
   * Loads json data from a project
   *
   * @param string $project_name
   *   The project name to search in.
   * @param string $json_file
   *   The name of the json file to load
   * @return array
   */
  public function projectJsonFile($project_name, $json_file) {
    $dir = $this->projectPath($project_name);
    $contents = file_get_contents("$dir/$json_file");
    return json_decode($contents, true);
  }

  /**
   * Load the composer.json data from a project
   */
  public function projectComposerJson($project_name) {
    return $this->projectJsonFile($project_name, 'composer.json');
  }

  /**
   * Load the composer.lock data from a project
   */
  public function projectComposerLock($project_name) {
    return $this->projectJsonFile($project_name, 'composer.lock');
  }

}
