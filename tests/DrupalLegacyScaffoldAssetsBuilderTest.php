<?php

namespace PackageGenerator;

use PackageGenerator\Builder\DrupalLegacyScaffoldAssetsBuilder;
use PackageGenerator\Util\DrupalCoreComposer;
use PHPUnit\Framework\TestCase;
use org\bovigo\vfs\vfsStream;

/**
 * Test our 'help' command.
 */
class DrupalLegacyScaffoldAssetsBuilderTest extends TestCase
{
  public function drupalLegacyScaffoldAssetsBuilderTestData() {
    return [
      [
        '8.8.x',
        [
          'name' => 'drupal/legacy-scaffold-assets',
          'type' => 'library',
          'description' => 'Scaffold assets from drupal/drupal, for use with versions 8.0.x - 8.7.x',
          'license' => 'GPL-2.0-or-later',
          'require' => [
            'drupal/core' => 'self.version',
          ],
          'extra' => [
            'drupal-scaffold' => [
              'allowed-packages' => [ 'drupal/core' ],
            ],
          ],
        ],
        [],
      ],

      [
        '8.7.7',
        [
          'name' => 'drupal/legacy-scaffold-assets',
          'type' => 'library',
          'description' => 'Scaffold assets from drupal/drupal, for use with versions 8.0.x - 8.7.x',
          'license' => 'GPL-2.0-or-later',
          'require' => [
            'drupal/core' => 'self.version',
          ],
          'extra' => [
            'drupal-scaffold' => [
              'file-mapping' => [
                '[web-root]/index.php' => 'assets/index.php',
              ],
            ],
          ],
        ],
        [
          'assets/index.php',
        ]
      ],
    ];
  }

  /**
   * @dataProvider drupalLegacyScaffoldAssetsBuilderTestData
   */
  public function testDrupalLegacyScaffoldAssetsBuilder($referenceName, $expected, $expectedAssets) {
    $fixtures = new Fixtures();
    $commitHash = 'sha1';

    $repositoryPath = $fixtures->projectPath($referenceName);
    $drupalCoreInfo = new DrupalCoreComposer($repositoryPath);
    $composer_metadata = $fixtures->projectComposerJson('scaffold-assets-metadata');

    $config = [
      'composer' => [
        'metadata' => $composer_metadata,
        'file-mapping' => [
          "[web-root]/index.php" => "assets/index.php",
          "[web-root]/no-such-file.txt" => "assets/no-such-file.txt",
        ]
      ]
    ];

    $require_dev_builder = new DrupalLegacyScaffoldAssetsBuilder($drupalCoreInfo, $referenceName, $commitHash, $config);

    $generatedJson = $require_dev_builder->getPackage();

    $this->assertEquals($expected, $generatedJson);

    $vfs = vfsStream::setup('scaffold');
    $targetBaseDir = vfsStream::url('scaffold');
    $placedFiles = $require_dev_builder->placeFiles($generatedJson, $targetBaseDir);
    sort($placedFiles);

    $this->assertEquals(implode(',', $expectedAssets), implode(',', $placedFiles));
    foreach ($expectedAssets as $expectedFile) {
      $this->assertTrue($vfs->hasChild($expectedFile), 'Does not have expected file ' . $expectedFile);
    }

  }
}
