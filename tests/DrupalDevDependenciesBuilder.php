<?php

namespace PackageGenerator;

use PackageGenerator\Builder\DrupalDevDependenciesBuilder;
use PackageGenerator\Util\DrupalCoreComposer;
use PHPUnit\Framework\TestCase;

/**
 * Test our 'help' command.
 */
class DrupalDevDependenciesBuilderTest extends TestCase
{
  public function drupalDevDependenciesBuilderTestData() {
    return [
      [
        '8.8.x',
        [
          'name' => 'drupal/core-dev',
          'type' => 'metapackage',
          'description' => 'require-dev dependencies from drupal/drupal; use in addition to drupal/core-recommended to run tests from drupal/core.',
          'license' => 'GPL-2.0-or-later',
          'require' =>
          [
            'behat/mink' => '1.8.0 | 1.7.1.1 | 1.7.x-dev',
            'behat/mink-goutte-driver' => '^1.2',
            'behat/mink-selenium2-driver' => '1.4.0 | 1.3.1.1 | 1.3.x-dev',
            'composer/composer' => '^1.8',
            'drupal/coder' => '^8.3.2',
            'jcalderonzumba/gastonjs' => '^1.0.2',
            'jcalderonzumba/mink-phantomjs-driver' => '^0.3.1',
            'justinrainbow/json-schema' => '^5.2',
            'mikey179/vfsstream' => '^1.2',
            'phpspec/prophecy' => '^1.7',
            'phpunit/phpunit' => '^6.5',
            'symfony/css-selector' => '^3.4.0',
            'symfony/debug' => '^3.4.0',
            'symfony/phpunit-bridge' => '^3.4.3',
          ],
          'conflict' =>
          [
            'drupal/core-dev-pinned' => '*',
            'webflo/drupal-core-require-dev' => '*',
          ],
        ]
      ],

      [
        '8.7.7',
        [
          'name' => 'drupal/core-dev',
          'type' => 'metapackage',
          'description' => 'require-dev dependencies from drupal/drupal; use in addition to drupal/core-recommended to run tests from drupal/core.',
          'license' => 'GPL-2.0-or-later',
          'require' =>
          [
            'behat/mink' => '1.8.0 | 1.7.1.1 | 1.7.x-dev',
            'behat/mink-goutte-driver' => '^1.2',
            'behat/mink-selenium2-driver' => '1.4.0 | 1.3.1.1 | 1.3.x-dev',
            'drupal/coder' => '^8.3.1',
            'jcalderonzumba/gastonjs' => '^1.0.2',
            'jcalderonzumba/mink-phantomjs-driver' => '^0.3.1',
            'justinrainbow/json-schema' => '^5.2',
            'mikey179/vfsstream' => '^1.2',
            'phpspec/prophecy' => '^1.7',
            'phpunit/phpunit' => '^4.8.35 || ^6.5',
            'symfony/css-selector' => '^3.4.0',
            'symfony/debug' => '^3.4.0',
            'symfony/phpunit-bridge' => '^3.4.3',
          ],
          'conflict' =>
          [
            'drupal/core-dev-pinned' => '*',
            'webflo/drupal-core-require-dev' => '*',
          ],
        ]
      ],
    ];
  }

  /**
   * @dataProvider drupalDevDependenciesBuilderTestData
   */
  public function testDrupalDevDependenciesBuilder($referenceName, $expected) {
    $fixtures = new Fixtures();
    $commitHash = 'sha1';

    $repositoryPath = $fixtures->projectPath($referenceName);
    $drupalCoreInfo = new DrupalCoreComposer($repositoryPath);
    $composer_metadata = $fixtures->projectComposerJson('core-dev-metadata');

    $config = [
      'composer' => [
        'metadata' => $composer_metadata,
      ]
    ];

    $require_dev_builder = new DrupalDevDependenciesBuilder($drupalCoreInfo, $referenceName, $commitHash, $config);

    $generatedJson = $require_dev_builder->getPackage();

    $this->assertEquals($expected, $generatedJson);

  }
}
