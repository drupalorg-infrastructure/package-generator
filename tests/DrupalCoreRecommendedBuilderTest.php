<?php

namespace PackageGenerator;

use PackageGenerator\Builder\DrupalCoreRecommendedBuilder;
use PackageGenerator\Util\DrupalCoreComposer;
use PHPUnit\Framework\TestCase;

/**
 * Test our 'help' command.
 */
class DrupalCoreRecommendedBuilderTest extends TestCase
{
  public function drupalCoreRecommendedBuilderTestData() {
    return [
      [
        '8.8.x',
        [
          'name' => 'drupal/core-recommended',
          'type' => 'metapackage',
          'description' => 'Locked core dependencies; require this project INSTEAD OF drupal/core.',
          'license' => 'GPL-2.0-or-later',
          'require' =>
          [
            'drupal/core' => 'self.version',
            'asm89/stack-cors' => '1.2.0',
            'brumann/polyfill-unserialize' => 'v1.0.3',
            'composer/installers' => 'v1.6.0',
            'composer/semver' => '1.5.0',
            'doctrine/annotations' => 'v1.2.7',
            'doctrine/cache' => 'v1.6.2',
            'doctrine/collections' => 'v1.3.0',
            'doctrine/common' => 'v2.6.2',
            'doctrine/inflector' => 'v1.1.0',
            'doctrine/lexer' => 'v1.0.1',
            'easyrdf/easyrdf' => '0.9.1',
            'egulias/email-validator' => '2.1.7',
            'guzzlehttp/guzzle' => '6.3.3',
            'guzzlehttp/promises' => 'v1.3.1',
            'guzzlehttp/psr7' => '1.4.2',
            'masterminds/html5' => '2.3.0',
            'paragonie/random_compat' => 'v2.0.18',
            'pear/archive_tar' => '1.4.6',
            'pear/console_getopt' => 'v1.4.1',
            'pear/pear-core-minimal' => 'v1.10.7',
            'pear/pear_exception' => 'v1.0.0',
            'psr/container' => '1.0.0',
            'psr/http-message' => '1.0.1',
            'psr/log' => '1.0.2',
            'stack/builder' => 'v1.0.5',
            'symfony-cmf/routing' => '1.4.1',
            'symfony/class-loader' => 'v3.4.26',
            'symfony/console' => 'v3.4.26',
            'symfony/debug' => 'v3.4.26',
            'symfony/dependency-injection' => 'v3.4.26',
            'symfony/event-dispatcher' => 'v3.4.26',
            'symfony/http-foundation' => 'v3.4.27',
            'symfony/http-kernel' => 'v3.4.26',
            'symfony/polyfill-ctype' => 'v1.11.0',
            'symfony/polyfill-iconv' => 'v1.11.0',
            'symfony/polyfill-mbstring' => 'v1.11.0',
            'symfony/polyfill-php70' => 'v1.11.0',
            'symfony/process' => 'v3.4.26',
            'symfony/psr-http-message-bridge' => 'v1.1.2',
            'symfony/routing' => 'v3.4.26',
            'symfony/serializer' => 'v3.4.26',
            'symfony/translation' => 'v3.4.26',
            'symfony/validator' => 'v3.4.26',
            'symfony/yaml' => 'v3.4.26',
            'twig/twig' => 'v1.38.4',
            'typo3/phar-stream-wrapper' => 'v2.1.2',
            'zendframework/zend-diactoros' => '1.4.1',
            'zendframework/zend-escaper' => '2.5.2',
            'zendframework/zend-feed' => '2.7.0',
            'zendframework/zend-stdlib' => '3.0.1',
          ],
          'conflict' =>
          [
            'webflo/drupal-core-strict' => '*',
          ],
        ]
      ],

      [
        '8.7.7',
        [
          'name' => 'drupal/core-recommended',
          'type' => 'metapackage',
          'description' => 'Locked core dependencies; require this project INSTEAD OF drupal/core.',
          'license' => 'GPL-2.0-or-later',
          'require' =>
          [
            'drupal/legacy-scaffold-assets' => 'self.version',
            'asm89/stack-cors' => '1.2.0',
            'brumann/polyfill-unserialize' => 'v1.0.3',
            'composer/installers' => 'v1.6.0',
            'composer/semver' => '1.5.0',
            'doctrine/annotations' => 'v1.2.7',
            'doctrine/cache' => 'v1.6.2',
            'doctrine/collections' => 'v1.3.0',
            'doctrine/common' => 'v2.6.2',
            'doctrine/inflector' => 'v1.1.0',
            'doctrine/lexer' => 'v1.0.1',
            'easyrdf/easyrdf' => '0.9.1',
            'egulias/email-validator' => '2.1.7',
            'guzzlehttp/guzzle' => '6.3.3',
            'guzzlehttp/promises' => 'v1.3.1',
            'guzzlehttp/psr7' => '1.4.2',
            'masterminds/html5' => '2.3.0',
            'paragonie/random_compat' => 'v2.0.18',
            'pear/archive_tar' => '1.4.6',
            'pear/console_getopt' => 'v1.4.1',
            'pear/pear-core-minimal' => 'v1.10.7',
            'pear/pear_exception' => 'v1.0.0',
            'psr/container' => '1.0.0',
            'psr/http-message' => '1.0.1',
            'psr/log' => '1.0.2',
            'stack/builder' => 'v1.0.5',
            'symfony-cmf/routing' => '1.4.1',
            'symfony/class-loader' => 'v3.4.26',
            'symfony/console' => 'v3.4.26',
            'symfony/debug' => 'v3.4.26',
            'symfony/dependency-injection' => 'v3.4.26',
            'symfony/event-dispatcher' => 'v3.4.26',
            'symfony/http-foundation' => 'v3.4.27',
            'symfony/http-kernel' => 'v3.4.26',
            'symfony/polyfill-ctype' => 'v1.11.0',
            'symfony/polyfill-iconv' => 'v1.11.0',
            'symfony/polyfill-mbstring' => 'v1.11.0',
            'symfony/polyfill-php70' => 'v1.11.0',
            'symfony/process' => 'v3.4.26',
            'symfony/psr-http-message-bridge' => 'v1.1.2',
            'symfony/routing' => 'v3.4.26',
            'symfony/serializer' => 'v3.4.26',
            'symfony/translation' => 'v3.4.26',
            'symfony/validator' => 'v3.4.26',
            'symfony/yaml' => 'v3.4.26',
            'twig/twig' => 'v1.38.4',
            'typo3/phar-stream-wrapper' => 'v2.1.2',
            'wikimedia/composer-merge-plugin' => 'v1.4.1',
            'zendframework/zend-diactoros' => '1.4.1',
            'zendframework/zend-escaper' => '2.5.2',
            'zendframework/zend-feed' => '2.7.0',
            'zendframework/zend-stdlib' => '3.0.1',
          ],
          'conflict' =>
          [
            'webflo/drupal-core-strict' => '*',
          ],
        ]
      ],
    ];
  }

  /**
   * @dataProvider drupalCoreRecommendedBuilderTestData
   */
  public function testDrupalCoreRecommendedBuilder($referenceName, $expected) {
    $fixtures = new Fixtures();
    $commitHash = 'sha1';

    $repositoryPath = $fixtures->projectPath($referenceName);
    $drupalCoreInfo = new DrupalCoreComposer($repositoryPath);
    $composer_metadata = $fixtures->projectComposerJson('core-recommended-metadata');

    $config = [
      'composer' => [
        'metadata' => $composer_metadata,
        'remove' => [
          '^8.8' => [
            'wikimedia/composer-merge-plugin',
          ],
        ],
      ]
    ];

    $require_dev_builder = new DrupalCoreRecommendedBuilder($drupalCoreInfo, $referenceName, $commitHash, $config);

    $generatedJson = $require_dev_builder->getPackage();

    $this->assertEquals($expected, $generatedJson);

  }
}
