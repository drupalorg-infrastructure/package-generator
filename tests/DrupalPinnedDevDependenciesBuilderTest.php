<?php

namespace PackageGenerator;

use PackageGenerator\Builder\DrupalPinnedDevDependenciesBuilder;
use PackageGenerator\Util\DrupalCoreComposer;
use PHPUnit\Framework\TestCase;

/**
 * Test our 'help' command.
 */
class DrupalPinnedDevDependenciedBuilderTest extends TestCase
{
  public function drupalPinnedDevDependenciedBuilderTestData() {
    return [
      [
        '8.8.x',
        [
          'name' => 'drupal/core-dev-pinned',
          'type' => 'metapackage',
          'description' => 'Pinned require-dev dependencies from drupal/drupal; use in addition to drupal/core-recommended to run tests from drupal/core.',
          'license' => 'GPL-2.0-or-later',
          'require' =>
          [
            'drupal/core' => 'self.version',
            'behat/mink' => '1.8.0 | 1.7.1.1 | 1.7.x-dev',
            'behat/mink-browserkit-driver' => '1.3.3',
            'behat/mink-goutte-driver' => 'v1.2.1',
            'behat/mink-selenium2-driver' => '1.4.0 | 1.3.1.1 | 1.3.x-dev',
            'composer/ca-bundle' => '1.1.4',
            'composer/composer' => '1.8.5',
            'composer/spdx-licenses' => '1.5.1',
            'composer/xdebug-handler' => '1.3.3',
            'doctrine/instantiator' => '1.0.5',
            'drupal/coder' => '8.3.6',
            'fabpot/goutte' => 'v3.2.3',
            'instaclick/php-webdriver' => '1.4.5',
            'ircmaxell/password-compat' => 'v1.0.4',
            'jcalderonzumba/gastonjs' => 'v1.0.2',
            'jcalderonzumba/mink-phantomjs-driver' => 'v0.3.2',
            'justinrainbow/json-schema' => '5.2.8',
            'mikey179/vfsstream' => 'v1.6.5',
            'myclabs/deep-copy' => '1.7.0',
            'phar-io/manifest' => '1.0.1',
            'phar-io/version' => '1.0.1',
            'phpdocumentor/reflection-docblock' => '2.0.4',
            'phpspec/prophecy' => 'v1.7.0',
            'phpunit/php-code-coverage' => '5.3.2',
            'phpunit/php-file-iterator' => '1.4.5',
            'phpunit/php-text-template' => '1.2.1',
            'phpunit/php-timer' => '1.0.9',
            'phpunit/php-token-stream' => '2.0.2',
            'phpunit/phpunit' => '6.5.14',
            'phpunit/phpunit-mock-objects' => '5.0.10',
            'sebastian/code-unit-reverse-lookup' => '1.0.1',
            'sebastian/comparator' => '2.1.3',
            'sebastian/diff' => '2.0.1',
            'sebastian/environment' => '3.1.0',
            'sebastian/exporter' => '3.1.0',
            'sebastian/global-state' => '2.0.0',
            'sebastian/object-enumerator' => '3.0.3',
            'sebastian/object-reflector' => '1.1.1',
            'sebastian/recursion-context' => '3.0.0',
            'sebastian/resource-operations' => '1.0.0',
            'sebastian/version' => '2.0.1',
            'seld/jsonlint' => '1.7.1',
            'seld/phar-utils' => '1.0.1',
            'squizlabs/php_codesniffer' => '3.4.2',
            'symfony/browser-kit' => 'v3.4.26',
            'symfony/css-selector' => 'v3.4.26',
            'symfony/dom-crawler' => 'v3.4.26',
            'symfony/filesystem' => 'v3.4.28',
            'symfony/finder' => 'v3.4.28',
            'symfony/phpunit-bridge' => 'v3.4.26',
            'theseer/tokenizer' => '1.1.2',
          ],
          'conflict' =>
          [
            'drupal/dev-dependencies' => '*',
            'webflo/drupal-core-require-dev' => '*',
          ],
        ]
      ],

      [
        '8.7.7',
        [
          'name' => 'drupal/core-dev-pinned',
          'type' => 'metapackage',
          'description' => 'Pinned require-dev dependencies from drupal/drupal; use in addition to drupal/core-recommended to run tests from drupal/core.',
          'license' => 'GPL-2.0-or-later',
          'require' =>
          [
            'drupal/core' => 'self.version',
            'behat/mink' => '1.8.0 | 1.7.1.1 | 1.7.x-dev',
            'behat/mink-browserkit-driver' => '1.3.3',
            'behat/mink-goutte-driver' => 'v1.2.1',
            'behat/mink-selenium2-driver' => '1.4.0 | 1.3.1.1 | 1.3.x-dev',
            'doctrine/instantiator' => '1.0.5',
            'drupal/coder' => '8.3.1',
            'fabpot/goutte' => 'v3.2.3',
            'instaclick/php-webdriver' => '1.4.5',
            'ircmaxell/password-compat' => 'v1.0.4',
            'jcalderonzumba/gastonjs' => 'v1.0.2',
            'jcalderonzumba/mink-phantomjs-driver' => 'v0.3.2',
            'justinrainbow/json-schema' => '5.2.8',
            'mikey179/vfsStream' => 'v1.6.5',
            'phpdocumentor/reflection-docblock' => '2.0.4',
            'phpspec/prophecy' => 'v1.7.0',
            'phpunit/php-code-coverage' => '2.2.4',
            'phpunit/php-file-iterator' => '1.4.5',
            'phpunit/php-text-template' => '1.2.1',
            'phpunit/php-timer' => '1.0.9',
            'phpunit/php-token-stream' => '1.4.12',
            'phpunit/phpunit' => '4.8.36',
            'phpunit/phpunit-mock-objects' => '2.3.8',
            'sebastian/comparator' => '1.2.4',
            'sebastian/diff' => '1.4.3',
            'sebastian/environment' => '1.3.8',
            'sebastian/exporter' => '1.2.2',
            'sebastian/global-state' => '1.1.1',
            'sebastian/recursion-context' => '1.0.5',
            'sebastian/version' => '1.0.6',
            'squizlabs/php_codesniffer' => '3.4.1',
            'symfony/browser-kit' => 'v3.4.26',
            'symfony/css-selector' => 'v3.4.26',
            'symfony/dom-crawler' => 'v3.4.26',
            'symfony/phpunit-bridge' => 'v3.4.26',
          ],
          'conflict' =>
          [
            'drupal/core-dev' => '*',
            'webflo/drupal-core-require-dev' => '*',
          ],
        ]
      ],
    ];
  }

  /**
   * @dataProvider drupalPinnedDevDependenciedBuilderTestData
   */
  public function testDrupalPinnedDevDependenciedBuilder($referenceName, $expected) {
    $fixtures = new Fixtures();
    $commitHash = 'sha1';

    $repositoryPath = $fixtures->projectPath($referenceName);
    $drupalCoreInfo = new DrupalCoreComposer($repositoryPath);
    $composer_metadata = $fixtures->projectComposerJson('core-dev-pinned-metadata');

    $config = [
      'composer' => [
        'metadata' => $composer_metadata,
      ]
    ];

    $require_dev_builder = new DrupalPinnedDevDependenciesBuilder($drupalCoreInfo, $referenceName, $commitHash, $config);

    $generatedJson = $require_dev_builder->getPackage();

    $this->assertEquals($expected, $generatedJson);

  }
}
