<?php

use Gitonomy\Git\Reference\Branch;
use Gitonomy\Git\Reference\Tag;
use Gitonomy\Git\Repository;
use PackageGenerator\BuilderInterface;
use PackageGenerator\Dumper;
use PackageGenerator\Util\DrupalCoreComposer;
use Robo\Tasks;
use Symfony\Component\Yaml\Yaml;

/**
 * This is project's console commands configuration for Robo task runner.
 *
 * @see http://robo.li/
 */
class RoboFile extends Tasks
{
  protected $clonedList = [];

  /**
   * @return \Robo\Collection\CollectionBuilder
   */
  public function generate($filepath, $options = ['push' => true, 'rewrite' => false, 'only' => ''])
  {
    if (!file_exists($filepath)) {
      throw new \LogicException('File not found.');
    }
    $config = Yaml::parse(file_get_contents($filepath));
    $collection = $this->collectionBuilder();
    foreach ($config['packages'] as $id => $job) {
      if ($this->isSelectedJob($id, $options['only'])) {
        $collection->addTask($this->buildPackage($id, $job, $options));
      }
    }
    return $collection;
  }

  protected function buildPackage($id, $config, $options)
  {
    $collection = $this->collectionBuilder();
    $source = 'build/' . hash('sha256', $config['source']);
    $target = 'build/' . hash('sha256', $config['target']);

    $collection->progressMessage("Build $id");
    $collection->progressMessage("Source is $source");

    if (!file_exists($source) && !array_key_exists($source, $this->clonedList)) {
      $this->clonedList[$source] = true;
      $collection->taskGitStack()
        ->cloneRepo($config['source'], $source);
    }

    $collection->progressMessage("Target is $target");

    if (!file_exists($target) && !array_key_exists($target, $this->clonedList)) {
      $this->clonedList[$target] = true;
      $collection->taskGitStack()
        ->cloneRepo($config['target'], $target);
    }

    $collection->addCode(function () use ($source, $target, $config, $options) {
      /**
       * @var Branch[] $branches
       */
      $branches = [];

      /**
       * @var Tag[] $tags
       */
      $tags = [];

      $repository = new Repository($source);
      $repository->run('fetch');

      $branches = array_filter($repository->getReferences()->getRemoteBranches(), function (Branch $branch) {
        if ($branch->isRemote() && preg_match('/^origin\/8\.[0-7]\./', $branch->getName(), $matches)) {
          return TRUE;
        }
        return FALSE;
      });

      $tags = array_filter($repository->getReferences()->getTags(), function (Tag $tag) {
        return preg_match('/^8\.[0-7]\.[0-9]+/', $tag->getName());
      });

      $refs = $tags + $branches;
      $refs_array = [];

      foreach ($refs as $ref) {
        $name = str_replace('origin/', '', $ref->getName());
        if ($ref instanceof Branch) {
          $name .= '-dev';
        }
        $refs_array[$name] = $ref;
      }

      $sorted = \Composer\Semver\Semver::sort(array_keys($refs_array));

      foreach ($sorted as $version) {
        /** @var \Gitonomy\Git\Reference\Tag|\Gitonomy\Git\Reference\Branch $ref */
        $ref = $refs_array[$version];
        $repository->run('reset', ['--hard', $ref->getCommitHash()]);

        $drupalCoreInfo = new DrupalCoreComposer($repository->getPath());

        // Create a new repository object so local references are up-to-date.
        $metapackage_repository = new Repository($target);
        $metapackage_repository->run('config', ['user.name', $config['git']['author']['name']]);
        $metapackage_repository->run('config', ['user.email', $config['git']['author']['email']]);

        $referenceName = $ref->getName();
        if ($ref instanceof Branch) {
          $referenceName = str_replace('origin/', '', $referenceName);
        }
        elseif (!($ref instanceof Tag)) {
          throw new \LogicException("gitObject should be Branch or Tag");
        }

        /** @var BuilderInterface $builder */
        $builder = new $config['builder'](
          $drupalCoreInfo,
          $referenceName,
          $ref->getCommitHash(),
          $config);

        $dump = new Dumper($ref, $builder, $metapackage_repository);
        $dump->allowRewrite($options['rewrite']);
        $dump->write();
      }
    });

    $collection->progressMessage("Done with $target");

    if ($options['push']) {
      $collection->taskGitStack()
        ->dir($target)
        ->exec(['push', '--all'])
        ->exec(['push', '--tags']);
    }

    return $collection;
  }

  protected function isSelectedJob($id, $jobList) {
    if (empty($jobList)) {
      return true;
    }

    foreach (explode(',', $jobList) as $oneJob) {
      if (strpos($id, $oneJob) !== false) {
        return true;
      }
    }

    return false;
  }
}
